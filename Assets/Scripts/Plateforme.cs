using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plateforme : MonoBehaviour
{

    
    enum State { FRESH = 0 , NORMAL = 1 , OLD = 2}
    [SerializeField]
    private Sprite[] m_sprites;

    private SpriteRenderer m_spriteRenderer;
    private State m_currentState;
    private State CurrentState
    {
        get => m_currentState;
        set
        {
            m_currentState = value;
            m_spriteRenderer.sprite = m_sprites[(int)CurrentState];
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        m_spriteRenderer = GetComponent<SpriteRenderer>();
        CurrentState = State.FRESH;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateState()
    {
        if (CurrentState == State.OLD)
        {
            Destroy(gameObject);
        } else
        {
            CurrentState++;
        }
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //Debug.Log("Normal is " + collision.contacts[0].normal);
            if (collision.contacts[0].normal == Vector2.down)
            {
                //Debug.Log("Player is on " + gameObject.GetInstanceID());
                collision.gameObject.transform.parent = gameObject.transform;
            }
        }
    }

  
    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && collision.gameObject.transform.parent == gameObject.transform)
        {
            collision.gameObject.transform.parent = null;
        }
    }

    private void OnDestroy()
    {
        PlayerController player;
        if(player = GetComponentInChildren<PlayerController>())
        {
            player.transform.parent = null;
        }
    }

}
