using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manage a follower to make him follow the path
/// </summary>
public class PathAI : MonoBehaviour
{
    /// <summary>
    /// The object which is following the path
    /// </summary>
    [SerializeField]
    GameObject m_follower;

    /// <summary>
    /// Follower Sprite Renderer
    /// </summary>
    [SerializeField]
    private SpriteRenderer spriteRenderer;

    /// <summary>
    /// Is the enemy in looping mode ? (else it is the turn back mode)
    /// </summary>
    [SerializeField]
    private bool m_isLooping = false;

    /// <summary>
    /// The enemy speed
    /// </summary>
    [SerializeField]
    private float m_speed = 1.0f;

    /// <summary>
    /// The acceptable distance to consider that a Waypoint is visited
    /// </summary>
    [SerializeField]
    private float deltaPath = 0.1f;

    /// <summary>
    /// The path parent containing the path
    /// </summary>
    [SerializeField]
    private GameObject m_pathParent;

    /// <summary>
    /// The Enemy path to follow
    /// </summary>
    private WayPoint[] m_path;

    /// <summary>
    /// Current index in the path array according to the WayPoint to visit
    /// </summary>
    private int m_currentIndex = 0;

    /// <summary>
    /// The path direction
    /// </summary>
    private int m_pathDirection = 1;

    // Awake is called when the script instance is being loaded.
    private void Awake()
    {
        if (spriteRenderer == null) spriteRenderer = GetComponent<SpriteRenderer>();
        m_path = m_pathParent.GetComponentsInChildren<WayPoint>();
    }

    /// <summary>
    /// Set the path follower
    /// </summary>
    /// <param name="go"> the new path follower </param>
    public void SetFollower(GameObject go)
    {
        m_follower = go;
    }

    /// <summary>
    /// Remove the current follower
    /// </summary>
    public void RemoveFollower()
    {
        m_follower = null;
    }

    // Update is called once per frame
    private void Update()
    {
        // No follower/path found
        if (m_follower == null || m_path.Length <= 0) return;

        WayPoint currentWayPoint;
        // Loop Mode
        if (m_isLooping)
        {
            // If the enemy is visiting the currentWayPoint
            if (Vector2.Distance(m_follower.transform.position, m_path[m_currentIndex].transform.position) < deltaPath)
            {
                m_currentIndex = (m_currentIndex + 1) % m_path.Length;
                currentWayPoint = m_path[m_currentIndex];
            }
            else
            {
                currentWayPoint = m_path[m_currentIndex];
            }
        }
        else // TurnBack Mode
        {
            // If the enemy is visiting the currentWayPoint
            if (Vector2.Distance(m_follower.transform.position, m_path[m_currentIndex].transform.position) < deltaPath)
            {
                // Is the enemy at the end or the beginning of the path ?
                if(m_currentIndex % m_path.Length == 0 || m_currentIndex % m_path.Length == m_path.Length - 1)
                    m_pathDirection = -m_pathDirection;
                m_currentIndex = Modulo((m_currentIndex + 1 * m_pathDirection), m_path.Length);
                currentWayPoint = m_path[m_currentIndex];
            }
            else
            {
                currentWayPoint = m_path[m_currentIndex];
            }
        }

        // Move the enemy towards his new destination (WayPoint)
        Vector2 nextPosition = Vector2.MoveTowards(m_follower.transform.localPosition, currentWayPoint.transform.position, m_speed * Time.deltaTime);
        if ((nextPosition.x - m_follower.transform.position.x > 0) != spriteRenderer.flipX)
            FlipFollower();
        m_follower.transform.localPosition = nextPosition;
    }

    /// <summary>
    /// Flip the follower in the good direction
    /// </summary>
    private void FlipFollower()
    {
        spriteRenderer.flipX = !spriteRenderer.flipX;
    }

    /// <summary>
    /// Modulo function which only returns a positive value
    /// </summary>
    /// <param name="a"> Dividend </param>
    /// <param name="n"> Divisor </param>
    /// <returns> Reminder </returns>
    int Modulo(int a, int n)
    {
        return (a % n + n) % n;
    }





    /// <summary>
    /// Draw gizmos in order to debug in play mode
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        if (Application.isPlaying)
        {
            if (m_path != null && m_path.Length > 0) Gizmos.DrawWireSphere(m_path[m_currentIndex].transform.position, 1);
        }
    }
}
