using UnityEngine;

public class EnemyKill : MonoBehaviour
{
    [SerializeField] private bool stickPlayer;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            AudioManager.Instance.PlayBatSound();
            if (stickPlayer)
                collision.GetComponent<Death>().Kill(true, gameObject);
            else
                collision.GetComponent<Death>().Kill(false, null);

            //If the killer is a projectile, destroy it
            if (gameObject.GetComponent<Projectile>())
            {
                Destroy(gameObject);
            }
        }

        //If a projectile hits the ground, destroy it
        if (gameObject.GetComponent<Projectile>() && collision.gameObject.layer == 6)
        {
            Destroy(gameObject);
        }

    }
}
