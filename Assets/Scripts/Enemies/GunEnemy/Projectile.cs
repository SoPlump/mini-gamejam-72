using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float force;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody2D>().AddForce(-this.transform.right * force);
    }

    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
}
