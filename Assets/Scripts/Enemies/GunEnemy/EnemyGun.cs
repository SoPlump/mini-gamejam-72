using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGun : MonoBehaviour
{

    [SerializeField] private GameObject prefab_projectile;
    [SerializeField] private float detectionRange;
    [SerializeField] private Transform targetPoint;
    [SerializeField] private float frqShoot;
    [SerializeField] private  Transform fireStart;
    [SerializeField] private Transform player;

    // Sources
    AudioSource soundSource;
    // Sounds Clips
    AudioClip fireClip;
    private float timeTemp;
    private Vector3 offset = new Vector3(0,0,0);
    private bool flip;

    // Start is called before the first frame update
    void Start()
    {
        soundSource = gameObject.AddComponent<AudioSource>();
        // Sound clips
        fireClip = (AudioClip)Resources.Load("Sounds/FireBall_sound");
        flip = false;
        timeTemp = frqShoot;
    }

    // Update is called once per frame
    void Update()
    {
        timeTemp -= Time.deltaTime;
        if (targetPoint == null)
        {
            if (timeTemp <= 0)
            {
                GameObject projectile = GameObject.Instantiate(prefab_projectile, fireStart.position - this.transform.right, Quaternion.identity);
                soundSource.PlayOneShot(fireClip, 0.5f);
                timeTemp = frqShoot;
            }
        }
        else
        {
            /*Get the target position*/
            Vector2 target_point = targetPoint.position;
            /*Check if the distance betwenn player and this*/
            if (Vector2.Distance(this.transform.position, player.position) < detectionRange)
            {
                //Debug.Log("Target on range");
                /*Orientation and flip*/
                Vector3 dir = targetPoint.position - this.transform.position;
                float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 180f;
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
                if (target_point.x > this.transform.position.x && !flip)
                {
                    flip = true;
                    Flip();
                }
                else if (target_point.x <= this.transform.position.x && flip)
                {
                    flip = false;
                    Flip();
                }   
                if (timeTemp <= 0)
                {
                    /*Fire*/
                    GameObject temp = Instantiate(prefab_projectile, fireStart.position - this.transform.right, Quaternion.identity);
                    soundSource.PlayOneShot(fireClip,0.5f);
                    temp.transform.right = -(target_point - new Vector2(transform.position.x, transform.position.y));
                    timeTemp = frqShoot;
                }
            }
        }
    }

    private void Flip()
    {
        this.transform.localScale = new Vector3(this.transform.localScale.x, -this.transform.localScale.y, this.transform.localScale.z);
    }
}
