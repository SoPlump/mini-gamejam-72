using UnityEngine;

public class EnemyCircle : MonoBehaviour
{
    [SerializeField] Transform point = null;
    [SerializeField] float radius = 1.0f;
    [SerializeField] float rotationSpeed = 2.0f;

    float m_angle;

    void Update()
    {
        m_angle += rotationSpeed * Time.deltaTime;

        Vector3 offset = new Vector3(Mathf.Sin(m_angle), Mathf.Cos(m_angle)) * radius;
        transform.position = point.position + offset;
    }
}
