using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


/// <summary>
/// Shake camera during a fixed time
/// </summary>
public class CameraShake : MonoBehaviour
{
    /// <summary>
    /// Script which make the camera following the player
    /// </summary>
    private CameraFollows m_cameraFollows;

    /// <summary>
    /// Shaking duration
    /// </summary>
    [Tooltip("Duration of the camera shake.")]
    [SerializeField]
    private float m_shakeDuration = 1.0f;

    /// <summary>
    /// Shaking amplitude
    /// </summary>
    [Tooltip("Force of the camera shake.")]
    [SerializeField]
    private float m_shakeAmplitude = 0.5f;

    /// <summary>
    /// Camera transform
    /// </summary>
    private Transform m_camTransform;


    /// <summary>
    /// Initial position
    /// </summary>
    private Vector3 m_originalPos;

    /// <summary>
    /// Is shaking ?
    /// </summary>
    private bool m_isShaking = false;

    void Awake()
    {
        m_cameraFollows = GetComponent<CameraFollows>();
        m_camTransform = transform;
    }

    /// <summary>
    /// Launch the shaking
    /// </summary>
    public void StartShaking()
    {
        StartCoroutine(Shake(m_shakeDuration));
    }

    /// <summary>
    /// Shake during <see cref="duration"/> seconds
    /// </summary>
    /// <param name="duration"> Shaking suration in seconds </param>
    /// <returns></returns>
    private IEnumerator Shake(float duration)
    {
        m_cameraFollows.IsFollowing = false;
        m_originalPos = m_camTransform.localPosition;
        m_isShaking = true;
        if(Gamepad.current != null)
        {
            Gamepad.current.SetMotorSpeeds(.3f, .3f);
        }
        yield return new WaitForSecondsRealtime(duration);
        m_isShaking = false;
        m_camTransform.localPosition = m_originalPos;
        if (Gamepad.current != null)
        {
            Gamepad.current.SetMotorSpeeds(0f, 0f);
        }
        m_cameraFollows.IsFollowing = true;
    }

    void Update()
    {
        if (m_isShaking)
        {
            m_camTransform.localPosition = m_originalPos + Random.insideUnitSphere * m_shakeAmplitude;
        }
    }
}