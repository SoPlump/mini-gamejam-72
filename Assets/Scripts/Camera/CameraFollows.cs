using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollows : MonoBehaviour
{
    [Tooltip("Object that the camera will follow.")]
    [SerializeField] private Transform m_target;
    [Tooltip("How quicly will the camera go to it's desired position.")]
    [SerializeField] private float m_smoothSpeed;
    [Tooltip("Initial offset relative to the target.")]
    [SerializeField] private Vector3 m_offset;
    private bool m_isFollowing = true;
    public bool IsFollowing
    {
        get => m_isFollowing;
        set => m_isFollowing = value;
    }

    private void FixedUpdate()
    {
        if (!m_isFollowing) return;
        Vector3 desiredPosition = m_target.position + m_offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, m_smoothSpeed * Time.deltaTime);
        transform.position = smoothedPosition;
    }

}
