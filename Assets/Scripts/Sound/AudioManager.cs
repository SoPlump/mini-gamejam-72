using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    //SINGLETON
    public static AudioManager Instance;

    // Sources

    AudioSource soundSource;

    // Sounds Clips

    AudioClip checkpointClip;
    AudioClip batDeathClip;
    AudioClip spikeClip;
    AudioClip fallClip;
    AudioClip waterClip;


    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        enabled = false;

        soundSource = gameObject.AddComponent<AudioSource>();
        // Sound clips
        checkpointClip = (AudioClip)Resources.Load("Sounds/checkpoint");
        batDeathClip = (AudioClip)Resources.Load("Sounds/scream_fun");
        spikeClip = (AudioClip)Resources.Load("Sounds/pikes_damn");
        fallClip = (AudioClip)Resources.Load("Sounds/fall_sound");
        waterClip = (AudioClip)Resources.Load("Sounds/water_splash");
    }


    // Sounds

    public void PlayCheckpointSound()
    {
        soundSource.PlayOneShot(checkpointClip);
    }

    public void PlayBatSound()
    {
        soundSource.PlayOneShot(batDeathClip);
    }

    public void PlaySinkSound()
    {
        soundSource.PlayOneShot(waterClip);
    }

    public void PlayPikesSound()
    {
        soundSource.PlayOneShot(spikeClip);
    }

    public void PlayFallSound()
    {
        soundSource.PlayOneShot(fallClip);
    }
}
