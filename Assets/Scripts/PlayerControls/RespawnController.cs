using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnController : MonoBehaviour
{
    private GameObject currentCP;

    [SerializeField]
    private GameObject prefab_CP;
    [SerializeField]
    private Vector3 offset = new Vector3(1, 0, 0);
    [SerializeField]
    private ParticleSystem respawnParticles;
    private Death death;

    private void Start()
    {
        if (death = FindObjectOfType<Death>())
        {
            death.SetCheckPointOnPlayer();
        }
    }

    public void SetRespawnPoint()
    {
        if (ScenarioManager.Instance.canSetCheckpoint)
        {
            //Destroy the last CP
            if (currentCP == null)
            {
                //Instantiate a prefab CP
                currentCP = Instantiate(prefab_CP, transform.position + offset, Quaternion.identity);
            }
            
            ParticleSystem respawn = Instantiate(respawnParticles, transform.position + offset, Quaternion.identity);
            Destroy(respawn.gameObject, respawn.main.duration);

            //Call UpdateCheckPointPosition(Transform newPos)
            death.UpdateCheckPointPosition(transform);

            currentCP.transform.SetPositionAndRotation(transform.position + offset, Quaternion.identity);
        }
    }
}
