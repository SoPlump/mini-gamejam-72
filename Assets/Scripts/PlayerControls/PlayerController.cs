using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D m_rb;
    private Animator m_animator;
    private bool m_isLookingRight = true;

    private RespawnController m_respawnController;

    [Space(10)]
    [Tooltip("Feet position for ground detection.")]
    [SerializeField] private Transform m_feetPos;
    [Tooltip("Layers considered as the ground.")]
    [SerializeField] private LayerMask m_whatIsGround;
    [Tooltip("Particles under the feet on walking.")]
    [SerializeField] private ParticleSystem m_trail;
    private float m_checkRadius = 0.3f;

    [Header("Move feel")]
    [Tooltip("Speed on which the player moves.")]
    [SerializeField] private float m_playerSpeed;
    private float m_axisX;
    private AudioSource m_walkingSound;

    [Header("Jump feel")]
    [Tooltip("Force of the players jump.")]
    [SerializeField] private float m_jumpForce;
    [Tooltip("Max duration of the ascending part of a jump.")]
    [SerializeField] private float m_jumpTime;
    private float m_jumpTimeCounter;
    private bool m_isJumping = false;
    private bool m_isGrounded = true;
    public bool IsGrounded
    {
        get => m_isGrounded;
        private set => m_isGrounded = value;
    }

    private void Start()
    {
        m_rb = GetComponent<Rigidbody2D>();
        m_animator = GetComponent<Animator>();
        m_respawnController = GetComponent<RespawnController>();
        m_walkingSound = GetComponent<AudioSource>();
        m_walkingSound.volume = 0.25f;
    }

    void Update()
    {
        if(Physics2D.OverlapCircle(m_feetPos.position, m_checkRadius, m_whatIsGround))
        {
            if (!m_isGrounded) m_trail.Play();
            m_isGrounded = true;
        }
        else
        {
            m_isGrounded = false;
        }

        m_animator.SetBool("IsJumping", m_isJumping);
        m_animator.SetBool("IsGrounded", m_isGrounded);
    }

    private void FixedUpdate()
    {
        if (m_isJumping)
        {
            m_animator.SetBool("IsJumping", true);

            if (m_jumpTimeCounter > 0)
            {
                m_rb.velocity = Vector2.up * m_jumpForce;
                m_jumpTimeCounter -= Time.deltaTime;
            }
            else
            {
                m_isJumping = false;
            }
        }

        m_rb.velocity = new Vector2(m_playerSpeed * m_axisX, m_rb.velocity.y);
    }

    private void OnStep()
    {
        if ((m_isGrounded))
        {
            m_walkingSound.PlayOneShot(m_walkingSound.clip);
            m_trail.Play();
        }
    }

    private void OnJump()
    {
        if (m_isGrounded)
        {
            m_jumpTimeCounter = m_jumpTime;
            m_isJumping = true;
        }
    }

    private void OnEndJump()
    {
        m_isJumping = false;
    }

    private void OnHorizontal(InputValue val)
    {
        m_axisX = val.Get<float>();

        if ((m_isLookingRight && m_axisX < 0) || (!m_isLookingRight && m_axisX > 0))
        {
            m_isLookingRight = !m_isLookingRight;
            Flip();
        }
        m_animator.SetFloat("Speed", Math.Abs(m_axisX));

    }

    private void OnCreateCheckpoint()
    {
        m_respawnController.SetRespawnPoint();
    }

    private void OnClean()
    {
        PlateformeManager.Instance.DestroyAllPlateformes();
    }

    private void OnRestart()
    {
        ScenarioManager.Instance.LoadAct(ScenarioManager.Instance.currentShortcut, ScenarioManager.Instance.currentAct);
    }

    public void OnPause()
    {
        FindObjectOfType<PauseMenu>().OnPause();
    }

    //Turn the player so he looks in the movement direction
    private void Flip()
    {
        Vector3 flipped = transform.localScale;
        flipped.x *= -1;
        transform.localScale = flipped;
    }
}
