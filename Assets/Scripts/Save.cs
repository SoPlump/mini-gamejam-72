using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class Save
{
    public bool[] isPlayed;
    public int[] scores;
    public int[] nbDeaths;
    public float[] times;
}
