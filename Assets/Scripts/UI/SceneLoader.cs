using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void LoadScene(string sceneToLoad)
    {
        SceneManager.LoadScene(sceneToLoad);
    }

    public IEnumerator LoadScene(ScenarioManager.Shortcuts sceneToLoad, int act)
    {
        string path = "";
        // Wait to enter concerned level
        switch ((int)sceneToLoad)
        {
            case 0:
                path = "Tutorial";
                break;

            case 1:
                path = "Level1";
                break;

            case 2:
                path = "Level2";
                break;
            default:
                break;
        }
        SceneManager.LoadScene(path);
        StartCoroutine(WaitAndLoad(sceneToLoad, act, path));
        yield return null;
    }

    IEnumerator WaitAndLoad(ScenarioManager.Shortcuts sceneToLoad, int act, string path)
    {
        yield return StartCoroutine(Gamemode.Instance.WaitForSceneLoad("Scenes/" + path));
        ScenarioManager.Instance.LoadAct(sceneToLoad, act);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
