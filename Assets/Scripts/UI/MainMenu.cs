using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public void LoadScene(string sceneName)
    {
        StartCoroutine(LoadSceneDelayed(sceneName, 0.3f));
    }

    private IEnumerator LoadSceneDelayed(string sceneName, float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        Gamemode.Instance.sceneLoader.LoadScene(sceneName);
    }

    public void Play()
    {
        StartCoroutine(Gamemode.Instance.sceneLoader.LoadScene(0, 0));
    }

    public void Quit()
    {
        Application.Quit();
    }
}
