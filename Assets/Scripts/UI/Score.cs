using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Score : MonoBehaviour
{
    [SerializeField] private ScenarioManager.Shortcuts m_shortcut;
    [SerializeField] private int m_act;
    [SerializeField] private GameObject[] m_stars;
    [SerializeField] private TMP_Text m_nbDeath;
    [SerializeField] private TMP_Text m_time;

    public void UpdateScore()
    {
        int levelOffset = 0;
        switch (m_shortcut)
        {
            case ScenarioManager.Shortcuts.LEVEL1:
                levelOffset = 3;
                break;
            case ScenarioManager.Shortcuts.LEVEL2:
                levelOffset = 8;
                break;
        }

        if (Gamemode.Instance.isPlayed[levelOffset + m_act])
        {
            int score = Gamemode.Instance.scores[levelOffset + m_act];
            Debug.Log(score);
            for (int i = 0; i <= score; ++i)
            {
                m_stars[i].SetActive(true);
            }
            m_nbDeath.text = Gamemode.Instance.nbDeaths[levelOffset + m_act].ToString();
            TimeSpan timeSpan = TimeSpan.FromSeconds(Gamemode.Instance.times[levelOffset + m_act]);
            m_time.text = timeSpan.ToString(@"mm\:ss");
        }
        else
        {
            m_nbDeath.text = "";
            m_time.text = "";
        }
    }

    private void Update()
    {
        UpdateScore();
    }
}
