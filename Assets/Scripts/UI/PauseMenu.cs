using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour
{
    private float m_TimeScaleRef = 1f;
    private float m_VolumeRef = 1f;
    [SerializeField]
    private GameObject m_menu;
    private bool m_paused = false;
    public bool canPause = true;

    private IEnumerator PauseOn(float delay)
    {
        if (!canPause) yield break;
        m_TimeScaleRef = Time.timeScale;
        m_VolumeRef = AudioListener.volume;

        yield return new WaitForSecondsRealtime(delay);
        AudioListener.volume = 0.2f;
        Time.timeScale = 0.0f;
        m_menu.SetActive(true);
        UpdateSelection(GetComponentInChildren<Button>().gameObject);
    }


    public IEnumerator PauseOff(float delay)
    {
        if (!canPause) yield break;
        yield return new WaitForSecondsRealtime(delay);
        AudioListener.volume = m_VolumeRef;
        Time.timeScale = m_TimeScaleRef;
        transform.Find("ControlsMenu").gameObject.SetActive(false);
        m_menu.SetActive(false);
    }

    public void OnPause()
    {
        if (m_paused)
        {
            StartCoroutine(PauseOff(0.2f));
            m_paused = false;
        }
        else
        {
            StartCoroutine(PauseOn(0.0f));
            m_paused = true;
        }
    }

    public void GoBackToMenu()
    {
        StartCoroutine(LoadMenuDelayed(0.3f));
    }

    private IEnumerator LoadMenuDelayed(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);

        AudioListener.volume = m_VolumeRef;
        Time.timeScale = m_TimeScaleRef;

        ScenarioManager.Instance.StopAllCoroutines();
        Destroy(ScenarioManager.Instance.gameObject);
        Destroy(PlateformeManager.Instance.gameObject);

        Gamemode.Instance.sceneLoader.LoadScene("MainMenu");
    }

    public void Quit()
    {
        Gamemode.Instance.sceneLoader.Quit();
    }

    public void UpdateSelection(GameObject selectedGO)
    {
        EventSystem.current.SetSelectedGameObject(selectedGO);
    }
}
