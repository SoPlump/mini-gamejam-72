using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelSelection : MonoBehaviour
{
    private ScenarioManager.Shortcuts m_shortcut;
    private int m_selectedAct = 0;
    [SerializeField] private Image m_levelImage;

    public int SelectedAct
    {
        get => m_selectedAct;
        set => m_selectedAct = value;
    }

    public void UpdateImage(Image image)
    {
        m_levelImage.sprite = image.sprite;
    }

    public void UpdateShortcut(int shortcut)
    {
        m_shortcut = (ScenarioManager.Shortcuts)shortcut;
    }

    public void LoadSelectedLevel()
    {
        StartCoroutine(Gamemode.Instance.sceneLoader.LoadScene(m_shortcut, m_selectedAct));
    }

    public void LoadAct(int act)
    {
        StartCoroutine(Gamemode.Instance.sceneLoader.LoadScene(m_shortcut, act));
    }
}
