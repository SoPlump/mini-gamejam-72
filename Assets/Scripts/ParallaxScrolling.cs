using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxScrolling : MonoBehaviour
{
    private Vector2 m_previousCameraPosition;
    [SerializeField]
    private float m_intensity;

    private void Start()
    {
        m_previousCameraPosition = Camera.main.gameObject.transform.position;
    }

    private void Update()
    {
        transform.position = (Vector2)transform.position - ((Vector2)Camera.main.gameObject.transform.position-m_previousCameraPosition)*m_intensity;
        m_previousCameraPosition = Camera.main.gameObject.transform.position;
    }
}
