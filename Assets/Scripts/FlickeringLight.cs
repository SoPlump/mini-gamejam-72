using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickeringLight : MonoBehaviour
{
    [SerializeField]
    private float MaxReduction;
    [SerializeField]
    private float MaxIncrease;
    [SerializeField]
    private float RateDamping;
    [SerializeField]
    private float Strength;

    private Light _lightSource;
    private float _baseIntensity;

    // Start is called before the first frame update
    void Start()
    {
        _lightSource = GetComponent<Light>();
        _baseIntensity = _lightSource.intensity;
        StartCoroutine(DoFlicker());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator DoFlicker()
    {
        while (true)
        {
            _lightSource.intensity = Mathf.Lerp(_lightSource.intensity, Random.Range(_baseIntensity - MaxReduction, _baseIntensity + MaxIncrease), Strength * Time.deltaTime);
            yield return new WaitForSeconds(RateDamping);
        }
    }
}
