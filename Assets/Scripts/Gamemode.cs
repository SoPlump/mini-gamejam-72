using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gamemode : MonoBehaviour
{

    public static Gamemode Instance;

    private static int m_nbActs = 3 + 5 + 1;

    [System.NonSerialized]
    public SceneLoader sceneLoader;
    [System.NonSerialized]
    public bool[] isPlayed = new bool[m_nbActs];
    [System.NonSerialized]
    public int[] scores = new int[m_nbActs];
    [System.NonSerialized]
    public int[] nbDeaths = new int[m_nbActs];
    [System.NonSerialized]
    public float[] times = new float[m_nbActs];

    void Awake()
    {
        if (Instance == null)
        {
            sceneLoader = GetComponent<SceneLoader>();
            LoadGame();
            Instance = this;
            DontDestroyOnLoad(gameObject);
            enabled = false;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Load game
    public void LoadGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Save save = (Save)bf.Deserialize(file);
            file.Close();

            isPlayed = save.isPlayed;
            scores = save.scores;
            nbDeaths = save.nbDeaths;
            times = save.times;
        }
        else
        {
            Debug.Log("No saved found to load");
        }
    }

    // Save game
    public void SaveGame()
    {
        Save save = CreateSaveGameObject();

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        if (file.CanWrite)
        {
            bf.Serialize(file, save);
            file.Close();

            Debug.Log("Game saved");
        }
        else
        {
            Debug.LogError("Can't save");
        }
    }

    private Save CreateSaveGameObject()
    {
        Save save = new Save();

        save.isPlayed = isPlayed;
        save.scores = scores;
        save.times = times;
        save.nbDeaths = nbDeaths;

        return save;
    }

    public void DeleteSave()
    {
        File.Delete(Application.persistentDataPath + "/gamesave.save");
    }

    public IEnumerator WaitForSceneLoad(string scenePath)
    {
        int idScene = SceneUtility.GetBuildIndexByScenePath(scenePath);
        Debug.LogWarning("Avant " + idScene);
        while (SceneManager.GetActiveScene().buildIndex != idScene)
        {
            Debug.LogWarning("Avant " + idScene);
            yield return null;
        }
        Debug.LogWarning("Apres " + idScene);
    }

}
