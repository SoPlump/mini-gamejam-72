using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fan : MonoBehaviour
{
    [SerializeField]
    private PathAI m_pathAI;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            m_pathAI.SetFollower(collision.gameObject);
            collision.attachedRigidbody.gravityScale = 0;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        m_pathAI.RemoveFollower();
        collision.attachedRigidbody.gravityScale = 1;
    }
}
