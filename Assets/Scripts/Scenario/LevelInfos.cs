using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInfos : MonoBehaviour
{
    public int bestScoreThreshold;
    public int mediumScoreThreshold;

    private Death death;

    private void Start()
    {
        death = FindObjectOfType<Death>();
    }

    public void PrintScore()
    {
        int nbStar = 0;
        if (death.nbDeathSinceLastCheckpoint <= bestScoreThreshold)
        {
            nbStar = 2;
        }
        else if (death.nbDeathSinceLastCheckpoint <= mediumScoreThreshold)
        {
            nbStar = 1;
        }
        else
        {
            nbStar = 0;
        }

        ScenarioManager.Instance.PrintScore(nbStar);
        death.ResetDeathNumber();
    }
}
