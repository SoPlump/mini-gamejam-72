using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenarioLevel1 : Scenario
{
    private ReferencesLevel1 refsLevel1;

    private void Awake()
    {
        refsLevel1 = FindObjectOfType<ReferencesLevel1>();
    }

    public void ChooseScenarioFromAct()
    {
        switch (ScenarioManager.Instance.currentAct)
        {
            case 0:
                m_currentTutoAct = NoMansLandAct();
                break;
            case 1:
                m_currentTutoAct = FlappyBirdAct();
                break;
            case 2:
                m_currentTutoAct = CliffAct();
                break;
            case 3:
                m_currentTutoAct = WaterActEasy();
                break;
            case 4:
                m_currentTutoAct = WaterActHard();
                break;
        }
    }

    public override IEnumerator EntryPoint()
    {
        ChooseScenarioFromAct();

        if (m_death = FindObjectOfType<Death>())
        {
            // Wait until player finish tuto
            yield return StartCoroutine(m_currentTutoAct);

            ScenarioManager.Instance.canSetCheckpoint = true;
            ScenarioManager.Instance.currentShortcut = ScenarioManager.Shortcuts.LEVEL2;
            ScenarioManager.Instance.currentAct = 0;

            PlateformeManager.Instance.DestroyAllPlateformes();
            PlateformeManager.Instance.MaxPlateformesNumber = 3;

            SceneManager.LoadScene("Level2");
        }

        yield return null;
    }
    private IEnumerator NoMansLandAct()
    {
        ScenarioManager.Instance.currentAct = 0;

        m_death.UpdateCheckPointPosition(m_death.gameObject.transform);
        yield return ScenarioManager.Instance.WaitForEvent(refsLevel1.checkpoint1.GetComponent<FlagComponent>().onTrigger);
        m_death.UpdateCheckPointPosition(refsLevel1.checkpoint1.transform);

        // Show score
        refsLevel1.checkpoint1.GetComponent<LevelInfos>().PrintScore();
        yield return new WaitForSeconds(m_timeScore);
        ScenarioManager.Instance.HideScore();

        yield return StartCoroutine(FlappyBirdAct());
    }

    private IEnumerator FlappyBirdAct()
    {
        ScenarioManager.Instance.currentAct = 1;

        m_death.UpdateCheckPointPosition(refsLevel1.checkpoint1.transform);
        yield return ScenarioManager.Instance.WaitForEvent(refsLevel1.checkpoint2.GetComponent<FlagComponent>().onTrigger);
        m_death.UpdateCheckPointPosition(refsLevel1.checkpoint2.transform);

        // Show score
        refsLevel1.checkpoint2.GetComponent<LevelInfos>().PrintScore();
        yield return new WaitForSeconds(m_timeScore);
        ScenarioManager.Instance.HideScore();

        yield return StartCoroutine(CliffAct());
    }

    private IEnumerator CliffAct()
    {
        ScenarioManager.Instance.currentAct = 2;

        m_death.UpdateCheckPointPosition(refsLevel1.checkpoint2.transform);
        yield return ScenarioManager.Instance.WaitForEvent(refsLevel1.checkpoint3.GetComponent<FlagComponent>().onTrigger);
        m_death.UpdateCheckPointPosition(refsLevel1.checkpoint3.transform);

        // Show score
        refsLevel1.checkpoint3.GetComponent<LevelInfos>().PrintScore();
        yield return new WaitForSeconds(m_timeScore);
        ScenarioManager.Instance.HideScore();

        yield return StartCoroutine(WaterActEasy());
    }

    private IEnumerator WaterActEasy()
    {
        ScenarioManager.Instance.currentAct = 3;

        m_death.UpdateCheckPointPosition(refsLevel1.checkpoint3.transform);
        yield return ScenarioManager.Instance.WaitForEvent(refsLevel1.checkpoint4.GetComponent<FlagComponent>().onTrigger);
        m_death.UpdateCheckPointPosition(refsLevel1.checkpoint4.transform);

        // Show score
        refsLevel1.checkpoint4.GetComponent<LevelInfos>().PrintScore();
        yield return new WaitForSeconds(m_timeScore);
        ScenarioManager.Instance.HideScore();

        yield return StartCoroutine(WaterActHard());
    }

    private IEnumerator WaterActHard()
    {
        ScenarioManager.Instance.currentAct = 4;

        m_death.UpdateCheckPointPosition(refsLevel1.checkpoint4.transform);
        yield return ScenarioManager.Instance.WaitForEvent(refsLevel1.checkpoint5.GetComponent<FlagComponent>().onTrigger);
        m_death.UpdateCheckPointPosition(refsLevel1.checkpoint5.transform);

        // Show score
        refsLevel1.checkpoint5.GetComponent<LevelInfos>().PrintScore();
        yield return new WaitForSeconds(m_timeScore / 2.0f);
        ScenarioManager.Instance.HideScore();
    }
}
