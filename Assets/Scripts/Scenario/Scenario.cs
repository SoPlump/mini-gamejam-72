using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public abstract class Scenario : MonoBehaviour
{
    protected Death m_death;
    protected float m_timeScore = 3.0f;

    protected IEnumerator m_currentTutoAct = null;

    private void Start()
    {
        m_death = FindObjectOfType<Death>();
    }

    public virtual IEnumerator EntryPoint()
    {
        Debug.LogFormat("Launched {0}", GetType());
        yield return null;
    }

}
