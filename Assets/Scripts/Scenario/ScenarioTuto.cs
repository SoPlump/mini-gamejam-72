using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenarioTuto : Scenario
{

    private ReferencesTuto refsTuto;


    public void ChooseScenarioFromAct()
    {
        switch (ScenarioManager.Instance.currentAct)
        {
            case 0:
                m_currentTutoAct = FallAct();
                break;
            case 1:
                m_currentTutoAct = BatAct();
                break;
            case 2:
                m_currentTutoAct = WaterAct();
                break;
        }
    }


    private void Awake()
    {
        refsTuto = FindObjectOfType<ReferencesTuto>();
    }

    public override IEnumerator EntryPoint()
    {
        ChooseScenarioFromAct();
        if (m_death = FindObjectOfType<Death>())
        {
            // Wait until player finish tuto
            yield return StartCoroutine(m_currentTutoAct);

            ScenarioManager.Instance.currentShortcut = ScenarioManager.Shortcuts.LEVEL1;
            ScenarioManager.Instance.currentAct = 0;

            PlateformeManager.Instance.DestroyAllPlateformes();
            PlateformeManager.Instance.MaxPlateformesNumber = 3;

            SceneManager.LoadScene("Level1");
        }

        yield return null;
    }

    public IEnumerator FallAct()
    {
        Debug.Log("Fall");
        ScenarioManager.Instance.currentAct = 0;

        // Set Checkpoint
        m_death.UpdateCheckPointPosition(m_death.gameObject.transform);

        yield return ScenarioManager.Instance.WaitForEvent(refsTuto.checkpoint1.GetComponent<FlagComponent>().onTrigger);
        m_death.UpdateCheckPointPosition(refsTuto.checkpoint1.transform);

        // Show score
        refsTuto.checkpoint1.GetComponent<LevelInfos>().PrintScore();
        yield return new WaitForSeconds(m_timeScore);
        ScenarioManager.Instance.HideScore();

        // Ajouter dialogue de notre nemesis ?

        yield return StartCoroutine(BatAct());
    }
    public IEnumerator BatAct()
    {
        Debug.Log("Bat");
        ScenarioManager.Instance.currentAct = 1;

        // Set Checkpoint
        m_death.UpdateCheckPointPosition(refsTuto.checkpoint1.transform);

        yield return ScenarioManager.Instance.WaitForEvent(refsTuto.checkpoint2.GetComponent<FlagComponent>().onTrigger);
        m_death.UpdateCheckPointPosition(refsTuto.checkpoint2.transform);

        // Show score
        refsTuto.checkpoint2.GetComponent<LevelInfos>().PrintScore();
        yield return new WaitForSeconds(m_timeScore);
        ScenarioManager.Instance.HideScore();

        yield return StartCoroutine(WaterAct());
    }
    public IEnumerator WaterAct()
    {
        Debug.Log("Water");
        ScenarioManager.Instance.currentAct = 2;

        // Set Checkpoint
        m_death.UpdateCheckPointPosition(refsTuto.checkpoint2.transform);

        yield return ScenarioManager.Instance.WaitForEvent(refsTuto.checkpoint3.GetComponent<FlagComponent>().onTrigger);
        m_death.UpdateCheckPointPosition(refsTuto.checkpoint3.transform);

        // Show score
        refsTuto.checkpoint3.GetComponent<LevelInfos>().PrintScore();
        yield return new WaitForSeconds(m_timeScore / 2.0f);
        ScenarioManager.Instance.HideScore();

        yield return null;
    }
}
