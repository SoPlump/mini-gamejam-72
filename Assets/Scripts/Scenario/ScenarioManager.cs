using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScenarioManager : MonoBehaviour
{
    // Shortcuts
    public enum Shortcuts
    {
        TUTO,
        LEVEL1,
        LEVEL2,
        END,
    }


    // SINGLETON
    public static ScenarioManager Instance;

    // Scenario
    private Scenario m_currentScenario;
    public Shortcuts currentShortcut = Shortcuts.TUTO;
    [NonSerialized]
    public bool canSetCheckpoint = false;

    [NonSerialized]
    public int currentAct = 0;

    // Score (not really the place sorry)
    [SerializeField]
    public GameObject scoreScreen;
    public Text scoreText;
    public GameObject[] stars;
    private float m_timeForLevel = 0.0f;

    public void PrintScore(int score)
    {
        int levelOffset = 0;
        switch(currentShortcut)
        {
            case Shortcuts.LEVEL1:
                levelOffset = 3;
                break;
            case Shortcuts.LEVEL2:
                levelOffset = 8;
                break;
        }

        Gamemode.Instance.isPlayed[levelOffset + currentAct] = true;
        // Check if score best results
        if (Gamemode.Instance.scores[levelOffset + currentAct] < score)
            Gamemode.Instance.scores[levelOffset + currentAct] = score;
        if ((Gamemode.Instance.times[levelOffset + currentAct] == 0)||(Gamemode.Instance.times[levelOffset + currentAct] > m_timeForLevel))
            Gamemode.Instance.times[levelOffset + currentAct] = m_timeForLevel;
        if ((Gamemode.Instance.nbDeaths[levelOffset + currentAct] == 0) || (Gamemode.Instance.nbDeaths[levelOffset + currentAct] > FindObjectOfType<Death>().nbDeathSinceLastCheckpoint))
            Gamemode.Instance.nbDeaths[levelOffset + currentAct] = FindObjectOfType<Death>().nbDeathSinceLastCheckpoint;

        m_timeForLevel = 0.0f;

        scoreScreen.SetActive(true);
        for (int i = 0; i <= score; ++i)
        {
            stars[i].SetActive(true);
        }

        if (score == 0)
        {
            scoreText.text = "You could do better and die less...";
        }
        else if (score == 1)
        {
            scoreText.text = "Correct, but still not perfect..";
        }
        else if (score == 2)
        {
            scoreText.text = "You're good! You died the minimum amount possible!";
        }

        // Save
        Gamemode.Instance.SaveGame();
    }

    public void HideScore()
    {
        foreach(var star in stars)
        {
            star.SetActive(false);
        }

        scoreScreen.SetActive(false);
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
        
    // Start is called before the first frame update
    void Start()
    {
        ChooseScenarioFromShortcut();
        StartCoroutine(UpdateScenarioOnLevelSwitch());
    }

    private void Update()
    {
        m_timeForLevel += Time.deltaTime;
    }

    public void LoadAct(Shortcuts levelShortcut, int currentAct)
    {
        StopAllCoroutines();
        currentShortcut = levelShortcut;
        // Load level
        switch (currentShortcut)
        {
            case Shortcuts.TUTO:
                if(SceneManager.GetActiveScene().name.Equals("Tutorial"))
                {
                    SceneManager.LoadScene("Tutorial");
                }
                break;
            case Shortcuts.LEVEL1:
                if (SceneManager.GetActiveScene().name.Equals("Level1"))
                {
                    SceneManager.LoadScene("Level1");
                }
                break;
            case Shortcuts.LEVEL2:
                if (SceneManager.GetActiveScene().name.Equals("Level2"))
                {
                    SceneManager.LoadScene("Level2");
                }
                break;
        }

        Death death;
        if (death = FindObjectOfType<Death>())
        {
            death.ResetDeathNumber();
        }

        PlateformeManager.Instance.DestroyAllPlateformes();
        this.currentAct = currentAct;

        StartCoroutine(UpdateScenarioOnLevelSwitch());
    }

    public IEnumerator UpdateScenarioOnLevelSwitch()
    {
        yield return new WaitForSeconds(0.1f);

        ChooseScenarioFromShortcut();

        if (m_currentScenario)
        {
            IEnumerator currentLevel = m_currentScenario.EntryPoint();
            StartCoroutine(currentLevel);

            // Move player        
            Death death;
            if (death = FindObjectOfType<Death>())
            {
                //StartCoroutine(death.Respawn());
                death.Respawn();
            }

            yield return StartCoroutine(WaitForSceneSwitchFromCurrentScene(SceneManager.GetActiveScene().buildIndex));

            StopCoroutine(currentLevel);

            StartCoroutine(UpdateScenarioOnLevelSwitch());
        }
    }


    public void ChooseScenarioFromShortcut()
    {
        switch (currentShortcut)
        {
            case Shortcuts.TUTO:
                m_currentScenario = gameObject.AddComponent<ScenarioTuto>();
                break;
            case Shortcuts.LEVEL1:
                m_currentScenario = gameObject.AddComponent<ScenarioLevel1>();
                break;
            case Shortcuts.LEVEL2:
                canSetCheckpoint = true;
                m_currentScenario = gameObject.AddComponent<ScenarioLevel2>();
                break;
            case Shortcuts.END:
                m_currentScenario = null;
                break;
        }
    }


    public IEnumerator WaitForEvent(UnityEvent unityEvent)
    {
        var trigger = false;
        Action action = () => trigger = true;

        unityEvent.AddListener(action.Invoke);

        yield return new WaitUntil(() => trigger);

        unityEvent.RemoveListener(action.Invoke);
    }
    public IEnumerator WaitForSceneSwitchFromCurrentScene(int idCurrentScene)
    {
        while (SceneManager.GetActiveScene().buildIndex == idCurrentScene)
        {
            yield return null;
        }
    }

}
