using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private FlagComponent m_flagComponent;

    [SerializeField]
    private GameObject m_checkpointLights;
    [SerializeField]
    private Vector3 m_offset;

    // Start is called before the first frame update
    void Start()
    {
        enabled = false;

        m_flagComponent = GetComponent<FlagComponent>();
        m_flagComponent.onTrigger.AddListener(RaiseFlag);
    }

    public void RaiseFlag()
    {
        // Play audio
        AudioManager.Instance.PlayCheckpointSound();

        // Play particules
        var checkpointParticleSystem = Instantiate(m_checkpointLights, gameObject.transform.position + m_offset, Quaternion.identity);
        Destroy(checkpointParticleSystem, checkpointParticleSystem.GetComponent<ParticleSystem>().main.duration * 2);

    }
}
