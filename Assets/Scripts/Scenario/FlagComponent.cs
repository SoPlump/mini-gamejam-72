using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum ConditionType
{
    TRIGGER_ENTER,
    FLAG,
}

public class FlagComponent : MonoBehaviour
{
    [SerializeField]
    private ConditionType m_condition;
    public UnityEvent onTrigger;
    public bool canBeTriggeredMultipleTimes = false;
    private bool m_triggered = false;


    private void Awake()
    {
        if (onTrigger == null)
            onTrigger = new UnityEvent();
    }

    public void Trigger()
    {
        if ((canBeTriggeredMultipleTimes)||(!m_triggered))
        {
            m_triggered = true;
            onTrigger.Invoke();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if ((other.CompareTag("Player")) && (m_condition == ConditionType.TRIGGER_ENTER))
        {
            Trigger();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if ((other.CompareTag("Player")) && (m_condition == ConditionType.TRIGGER_ENTER))
        {
            Trigger();
        }
    }
}
