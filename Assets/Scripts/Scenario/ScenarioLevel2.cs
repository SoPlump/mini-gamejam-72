using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenarioLevel2 : Scenario
{
    private ReferencesLevel2 refsLevel2;

    private void Awake()
    {
        refsLevel2 = FindObjectOfType<ReferencesLevel2>();
    }

    public override IEnumerator EntryPoint()
    {
        if (m_death = FindObjectOfType<Death>())
        {
            ScenarioManager.Instance.currentAct = 0;

            m_death.UpdateCheckPointPosition(m_death.gameObject.transform);
            yield return ScenarioManager.Instance.WaitForEvent(refsLevel2.checkpoint1.GetComponent<FlagComponent>().onTrigger);
            m_death.UpdateCheckPointPosition(refsLevel2.checkpoint1.transform);

            // Show score
            refsLevel2.checkpoint1.GetComponent<LevelInfos>().PrintScore();
            yield return new WaitForSeconds(m_timeScore);
            ScenarioManager.Instance.HideScore();

            yield return new WaitForSeconds(m_timeScore / 2.0f);
            ScenarioManager.Instance.currentShortcut = ScenarioManager.Shortcuts.END;

            PlateformeManager.Instance.DestroyAllPlateformes();
            PlateformeManager.Instance.MaxPlateformesNumber = 3;

            SceneManager.LoadScene("FinalDialogues");
        }

        yield return null;
    }
}
