using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
    private AudioSource m_audioSource;

    [SerializeField]
    private GameObject m_waterSplash;

    //// Start is called before the first frame update
    //void Start()
    //{
    //    m_audioSource = GetComponent<AudioSource>();
    //}

    //// Update is called once per frame
    //void Update()
    //{
        
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            AudioManager.Instance.PlaySinkSound();
            var waterSplashSystem = Instantiate(m_waterSplash, collision.transform.position, Quaternion.identity);
            Destroy(waterSplashSystem, waterSplashSystem.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
            collision.GetComponent<Death>().Kill(false, gameObject, false);
        }
        Plateforme deadBody;
        if (deadBody = collision.GetComponent<Plateforme>())
        {
            deadBody.gameObject.GetComponent<Rigidbody2D>().drag = 8;
        }
    }

}
