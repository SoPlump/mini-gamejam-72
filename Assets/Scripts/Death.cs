using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour
{
    private CameraShake m_cameraShake;

    //Particle systems
    [SerializeField]
    private GameObject m_blood;
    [SerializeField]
    private GameObject m_reviveLights;
    [SerializeField]
    private GameObject m_reviveSparkles;

    private Vector2 m_checkpoint;

    // Score
    public int nbDeathSinceLastCheckpoint = 0;

    public Vector2 Checkpoint
    {
        get => m_checkpoint;
        set => m_checkpoint = value;
    }

    void Awake()
    {
        Checkpoint = transform.position;
        m_cameraShake = Camera.main.GetComponent<CameraShake>();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -20.0f) Fall();
    }

    // When player is fallen in the void
    public void Fall()
    {
        ++nbDeathSinceLastCheckpoint;

        m_cameraShake.StartShaking();

        AudioManager.Instance.PlayFallSound();

        //StartCoroutine(Respawn());
        Respawn();
    }

    // if fixePlatform is true, the dead body / platform will not be affected by physics
    // if killer is not null , the plateforme will stick to it
    public void Kill(bool fixedPlatform, GameObject killer = null, bool isDeathBloody = true) 
    {
        ++nbDeathSinceLastCheckpoint;

        m_cameraShake.StartShaking();

        if (isDeathBloody)
        {
            var bloodParticleSystem = Instantiate(m_blood, GetComponent<Rigidbody2D>().transform.position, Quaternion.Euler(0, 0, -90));
            Destroy(bloodParticleSystem, bloodParticleSystem.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
        }
        PlateformeManager.Instance.CreatePlateforme(transform.position, fixedPlatform, killer);
        //StartCoroutine(Respawn());
        Respawn();
    }

    public void Respawn()
    {
        GetComponent<Rigidbody2D>().transform.position = Checkpoint;
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
        //yield return new WaitForEndOfFrame();

        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        GetComponent<Rigidbody2D>().WakeUp();


        //yield return new WaitForEndOfFrame();

        var reviveLightsParticleSystem = Instantiate(m_reviveLights, Vector2.zero, Quaternion.identity);
        Destroy(reviveLightsParticleSystem, reviveLightsParticleSystem.GetComponent<ParticleSystem>().main.duration);
        reviveLightsParticleSystem.transform.SetParent(gameObject.transform, false);

        var reviveSparklesParticleSystem = Instantiate(m_reviveSparkles, Vector2.zero, Quaternion.identity);
        Destroy(reviveSparklesParticleSystem, reviveSparklesParticleSystem.GetComponent<ParticleSystem>().main.duration);
        reviveSparklesParticleSystem.transform.SetParent(gameObject.transform, false);

        //yield return null;
    }

    public void ResetDeathNumber()
    {
        nbDeathSinceLastCheckpoint = 0;
    }

    public void UpdateCheckPointPosition(Transform newPos)
    {
        Checkpoint = newPos.position;
    }

    public void SetCheckPointOnPlayer()
    {
        Checkpoint = gameObject.transform.position;
    }
}
