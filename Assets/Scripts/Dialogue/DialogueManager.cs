﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DialogueManager : MonoBehaviour
{
    //[SerializeField] GameObject hud = null;
    [SerializeField] GameObject dialogueBox = null;
    [SerializeField] Text dialogueName = null;
    [SerializeField] Image dialoguePortraitLeft = null;
    [SerializeField] Image dialoguePortraitRight = null;
    [SerializeField] Text dialogueText = null;
    [SerializeField] GameObject continueButton = null;
    [SerializeField] float characterWaitTime = 0.05f;

    readonly Queue<string> sentences = new Queue<string>();
    readonly Queue<DialogueActions.sentenceAction> sentenceActions = new Queue<DialogueActions.sentenceAction>();
    DialogueTrigger currentDialogueTrigger;
    static bool inDialogue = false;

    private void Start()
    {
        dialogueBox.SetActive(false);
    }

    public void StartDialogue(DialogueTrigger dialogueTrigger)
    {
        DisableControl();
        inDialogue = true;
        currentDialogueTrigger = dialogueTrigger;
        sentences.Clear();
        sentenceActions.Clear();

        FindObjectOfType<EventSystem>().SetSelectedGameObject(continueButton);

        if(dialogueTrigger.actionsEnabled)
        {
            dialogueName.text = dialogueTrigger.dialogueActions.name;
            if(dialogueTrigger.portraitLeft)
            {
                dialoguePortraitLeft.enabled = true;
                dialoguePortraitRight.enabled = false;
                dialoguePortraitLeft.sprite = dialogueTrigger.dialogueActions.portrait;
            }
            else
            {
                dialoguePortraitLeft.enabled = false;
                dialoguePortraitRight.enabled = true;
                dialoguePortraitRight.sprite = dialogueTrigger.dialogueActions.portrait;
            }
            foreach (DialogueActions.sentenceAction sentence in dialogueTrigger.dialogueActions.sentenceActions)
            {
                sentenceActions.Enqueue(sentence);
            }
        }
        else
        {
            dialogueName.text = dialogueTrigger.dialogue.name;
            dialoguePortraitLeft.sprite = dialogueTrigger.dialogue.portrait;
            foreach (string sentence in dialogueTrigger.dialogue.sentences)
            {
                sentences.Enqueue(sentence);
            }
        }

        dialogueBox.SetActive(true);
        
        

        NextSentence();
    }

    public void NextSentence()
    {
        if (currentDialogueTrigger.actionsEnabled)
        {
            if (sentenceActions.Count == 0)
            {
                EndDialogue();
                return;
            }

            DialogueActions.sentenceAction action = sentenceActions.Dequeue();
            StopAllCoroutines();
            StartCoroutine(SentenceCharacter(action.sentence));
            action.sentenceEvent.Invoke();
            if (dialogueText.text.Equals(""))
            {
                NextSentence();
            }
        }
        else
        {
            if (sentences.Count == 0)
            {
                EndDialogue();
                return;
            }

            string sentence = sentences.Dequeue();
            StopAllCoroutines();
            StartCoroutine(SentenceCharacter(sentence));
        }
    }

    IEnumerator SentenceCharacter(string sentence)
    {
        dialogueText.text = "";
        foreach (var chara in sentence)
        {
            dialogueText.text += chara;
            yield return new WaitForSecondsRealtime(characterWaitTime);
        }
    }

    private void EndDialogue()
    {
        EnableControl();
        dialogueBox.SetActive(false);
        inDialogue = false;

        currentDialogueTrigger.DialogueEnd();
    }

    private void EnableControl()
    {
        FindObjectOfType<PauseMenu>().canPause = true;
        Time.timeScale = 1;
    }

    private void DisableControl()
    {
        FindObjectOfType<PauseMenu>().canPause = false;
        Time.timeScale = 0;
    }

    public static bool IsInDialogue()
    {
        return inDialogue;
    }
}
