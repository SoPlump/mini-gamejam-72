using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImmediateDialogue : DialogueTrigger
{
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(0.01f);
        DialogueStart();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
