﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class DialogueWithTrigger : DialogueTrigger
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        DialogueStart();
    }

    public override void DialogueEnd()
    {
        
    }
}
