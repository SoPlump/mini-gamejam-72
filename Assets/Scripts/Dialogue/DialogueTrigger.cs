﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class DialogueTrigger : MonoBehaviour
{
    public Dialogue dialogue = null;
    public DialogueActions dialogueActions = null;
    public bool actionsEnabled;
    public bool portraitLeft = true;

    public void DialogueStart()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(this);
    }

    public virtual void NextSentence(int sentence) { }

    public virtual void DialogueEnd() { }
}
#if UNITY_EDITOR
[CustomEditor(typeof(DialogueTrigger), true)]
public class DialogueEditor : Editor
{
    SerializedProperty dialogue;
    SerializedProperty dialogueActions;

    private void OnEnable()
    {
        dialogue = serializedObject.FindProperty("dialogue");
        dialogueActions = serializedObject.FindProperty("dialogueActions");
    }

    public override void OnInspectorGUI()
    {
        DialogueTrigger dialogueTrigger = target as DialogueTrigger;
        dialogueTrigger.actionsEnabled = EditorGUILayout.Toggle("Actions?", dialogueTrigger.actionsEnabled);
        dialogueTrigger.portraitLeft = EditorGUILayout.Toggle("Portait Left?", dialogueTrigger.portraitLeft);
        if(dialogueTrigger.actionsEnabled)
        {
            EditorGUILayout.PropertyField(dialogueActions);
        }
        else
        {
            EditorGUILayout.PropertyField(dialogue);
        }

        serializedObject.ApplyModifiedProperties();
    }
}
#endif