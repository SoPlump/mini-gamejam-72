using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class DialogueActions
{
    [System.Serializable] public class SentenceEvent : UnityEvent { }

    [System.Serializable]
    public struct sentenceAction
    {
        [TextArea(3, 10)]
        public string sentence;
        public SentenceEvent sentenceEvent;
    }

    public string name;

    public Sprite portrait;

    public sentenceAction[] sentenceActions;
}
