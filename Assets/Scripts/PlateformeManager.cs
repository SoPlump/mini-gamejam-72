using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateformeManager : MonoBehaviour
{

    [SerializeField]
    private Plateforme m_plateformePrefab;

    [SerializeField]
    private uint m_maxPlateformesNumber;
    public uint MaxPlateformesNumber
    {
        get => m_maxPlateformesNumber;
        set => m_maxPlateformesNumber = value;
    }

    private List<Plateforme> m_activePlateformes = new List<Plateforme>();

    //SINGLETON
    public static PlateformeManager Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(TestCoroutine());
    }

    IEnumerator TestCoroutine()
    {
        yield return null;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreatePlateforme(Vector3 position, bool isFixed , GameObject parent)
    {
        UpdateAllPlateformes();
        //If max number of plateformes reached
        if (m_activePlateformes.Count >= m_maxPlateformesNumber)
        {
            //Remove oldest plateforme
            Plateforme oldest_plateforme = m_activePlateformes[0];
            DestroyPlateforme(oldest_plateforme);
        }
        //Create new Plateforme
        Plateforme newPlateforme = Instantiate(m_plateformePrefab, position, Quaternion.identity);
        if (parent != null)
        {
            newPlateforme.gameObject.transform.parent = parent.transform;
        }
        else
        {
            newPlateforme.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation; 
        }
        newPlateforme.GetComponent<Rigidbody2D>().isKinematic = isFixed;
        m_activePlateformes.Add(newPlateforme);

    }

    void DestroyPlateforme(Plateforme plateforme)
    {
        //Remove plateforme from the active plateformes list
        m_activePlateformes.Remove(plateforme);
        //Destroy object
        Destroy(plateforme.gameObject);
    }
    
    public void DestroyAllPlateformes()
    {
        for(int i = m_activePlateformes.Count - 1; i >= 0; i--)
        {
            DestroyPlateforme(m_activePlateformes[i]);
        }
    }

    void UpdateAllPlateformes()
    {
        foreach(Plateforme plateforme in m_activePlateformes)
        {
            plateforme.UpdateState();
        }
    }
}
